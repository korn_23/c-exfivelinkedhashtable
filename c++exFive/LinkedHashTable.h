#pragma once
#pragma once
#include<iostream>

struct NoElemException {
};


struct Element {
	int data;
	int key;
	Element *next;

	Element(int data, int key, Element *next = nullptr) {
		this->data = data;
		this->next = next;
		this->key = key;
	}
};

class HashList {
	private:
		Element *first;
		Element *last;
	public:
		friend class Iterator;

		HashList() {
			first = nullptr;
			last = nullptr;
		}

		~HashList() {
			Element *temp = first;
			while (temp != last) {
				temp = temp->next;
				delete first;
				first = temp;
			}
			delete first;
		}

		void addElem(int elem, int key) {
			if (!last) {
				last = new Element(elem, key);
				first = last;
			}
			else {
				last->next = new Element(elem, key);
				last = last->next;
			}
		}

		void delElem(int elem) {
			Element *temp = first->next;
			Element *buf = first;
			if (first->data == elem) {
				delete first;
				first = temp;
				return;
			}
			while (temp->data != elem) {
				buf = temp;
				temp = temp->next;
			}
			buf->next = temp->next;
			delete temp;
		}

		void doEmpty() {
			Element *temp = first;
			while (temp != last) {
				temp = temp->next;
				delete first;
				first = temp;
			}
			delete first;
		}
};

class LinkedHashTable {
	private:
		Element **hashTable;
		HashList *list;
		int size;
		int countElem;
	public:
		friend class Iterator;

		LinkedHashTable() {
			countElem = 0;
			size = 5;
			hashTable = new Element *[5];
			list = new HashList();
			for (int i = 0; i < size; i++) {
				hashTable[i] = nullptr;
			}
		}

		explicit LinkedHashTable(int size) {
			countElem = 0;
			this->size = size;
			hashTable = new Element *[size];
			list = new HashList();
			for (int i = 0; i < size; i++) {
				hashTable[i] = nullptr;
			}
		}

		void addElem(int elem, int key) {
			int position = key % size;
			if (!hashTable[position]) {
				hashTable[position] = new Element(elem, key);
			}
			else {
				Element *temp = hashTable[position];
				while (temp->next) {
					temp = temp->next;
				}
				temp->next = new Element(elem, key);
			}
			list->addElem(elem, key);
			countElem++;
		}

		void removeElem(int key) {
			int position = key % size;
			Element *temp = hashTable[position];
			Element *buf = temp;
			while (temp) {
				if (key == temp->key) {
					if (buf == temp) {
						hashTable[position] = temp->next;
					}
					else {
						buf->next = temp->next;
					}
					list->delElem(temp->data);
					delete temp;
					countElem--;
					return;
				}
				buf = temp;
				temp = temp->next;
			}
		}

		int getElem(int key) {
			int position = key % size;
			Element *temp = hashTable[position];
			while (temp) {
				if (key == temp->key) {
					return temp->data;
				}
				temp = temp->next;
			}
			throw NoElemException();
		}

		void doEmpty() {
			for (int i = 0; i < size; i++) {
				Element *temp = hashTable[i];
				Element *buf;
				while (temp) {
					buf = temp->next;
					delete temp;
					temp = buf;
				}
				hashTable[i] = nullptr;
			}
			countElem = 0;
			list->doEmpty();
		}

		bool isEmpty() {
			return countElem > 0;
		}

		void printElem() {
			for (int i = 0; i < size; i++) {
				Element *temp = hashTable[i];
				std::cout << i << ": ";
				while (temp) {
					std::cout << " " << temp->data << " ";
					temp = temp->next;
				}
				std::cout << std::endl;
			}
		}

		~LinkedHashTable() {
			doEmpty();
			delete[] hashTable;
			delete list;
		}
};


class Iterator {
	private:
		Element *now;
		LinkedHashTable *hashList;

	public:
		explicit Iterator(LinkedHashTable *&hashTable) {
			hashList = hashTable;
			now = hashList->list->first;
		}

		~Iterator() = default;

		bool hasNext() {
			return now != hashList->list->last;
		}


		void start() {
			now = hashList->list->first;
		}

		void next() {
			now = now->next;
		}

		int seeElem() {
			return now->data;
		}
	};